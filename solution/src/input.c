#include <stdbool.h>
#include "include/input.h"

bool open_file(FILE** file, const char* name, const char* mode) {
	*file = fopen(name, mode);
	if (!*file)
		return false;
	return true;
}

bool close_file(FILE* file) {
	if (fclose(file))
		return false;
	return true;
}

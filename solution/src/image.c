#include "include/image.h"
#include "mm_malloc.h"

struct image image_create(const uint64_t width, const uint64_t height) {
	struct pixel *pixels = malloc(sizeof(struct pixel) * width * height);
	return (struct image) {.height = height, .width = width, .pixels = pixels};
}

void image_destroy(const struct image* image) {
	free(image->pixels);
}

#ifndef IMAGE_H
#define IMAGE_H

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

struct __attribute__((packed)) pixel {
	uint8_t b, g, r;
};

struct image {
	uint64_t height, width;
	struct pixel* pixels;
};

struct image image_create(uint64_t height, uint64_t width);

void image_destroy(const struct image* image);

#endif //IMAGE_H

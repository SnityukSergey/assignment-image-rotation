#include "include/bmp.h"

#include <stdio.h>

#define BMP_TYPE 19778
#define BMP_PLANES 1
#define BMP_SIZE 40
#define BPC 24
#define COMPR 0
#define X_PPM 0
#define Y_PPM 0
#define IMP_COLORS 0
#define NUM_COLORS 0

static inline size_t get_padding(size_t width) {
	return (4 - (size_t)((width * sizeof(struct pixel)) % 4));
}

static size_t image_size(const struct image* image) {
	return (image->width * sizeof(struct pixel) + get_padding(image->width) * image->height);
}

static size_t file_size(const struct image* image) {
	return image_size(image) + sizeof(struct bmp_header);
}

static enum read_status get_header(FILE* file, struct bmp_header* bmpHeader) {
	return fread(bmpHeader, sizeof(struct bmp_header), 1, file);
}

static struct bmp_header create_header(const struct image* image) {
	return (struct bmp_header) {
			.bfType = BMP_TYPE,
			.bfileSize = file_size(image),
			.bfReserved = 0,
			.bOffBits = sizeof(struct bmp_header),
			.biSize = BMP_SIZE,
			.biWidth = image->width,
			.biHeight = image->height,
			.biPlanes = BMP_PLANES,
			.biBitCount = BPC,
			.biCompression = COMPR,
			.biSizeImage = image_size(image),
			.biXPelsPerMeter = X_PPM,
			.biYPelsPerMeter = Y_PPM,
			.biClrUsed = NUM_COLORS,
			.biClrImportant = IMP_COLORS
	};
}

enum read_status from_bmp(FILE* in_file, struct image* image) {
	struct bmp_header header = {0};
	if (!get_header(in_file, &header))
		return READ_INVALID_HEADER;
	if (header.bfType != 0x4D42)
		return READ_INVALID_SIGNATURE;
	*image = image_create(header.biHeight, header.biWidth);
	const size_t padding = get_padding(image->width);
	for (size_t i = 0; i < image->height; i++) {
		for (size_t j = 0; j < image->width; j++) {
			if (!fread(&(image->pixels[image->width * i + j]), sizeof(struct pixel), 1, in_file))
				return READ_ERROR;
		}
		if (fseek(in_file, (long) padding, SEEK_CUR))
			return READ_ERROR;
	}
	return READ_OK;
}

enum write_status to_bmp(FILE* out_file, const struct image* image) {
	struct bmp_header header = create_header(image);
	if (!fwrite(&header, sizeof(struct bmp_header), 1, out_file))
		return WRITE_ERROR;
	fseek(out_file, header.bOffBits, SEEK_SET);
	const int8_t zero = 0;
	const size_t padding = get_padding(image->width);
	if (image->pixels != NULL) {
		for (size_t i = 0; i < image->height; i++) {
			if (fwrite(image->pixels + i * image->width, sizeof(struct pixel), image->width, out_file) != image->width)
				return WRITE_ERROR;
			for (size_t j = 0; j < padding; ++j) {
				if (fwrite(&zero, 1, 1, out_file) != 1)
					return WRITE_ERROR;
			}
		}
	} else {
		return WRITE_ERROR;
	}
	return WRITE_OK;
}

#include "include/rotate.h"
#include <mm_malloc.h>

static uint64_t pixel_index_output(size_t i, size_t j, struct image image) {
	return image.height * j + (image.height - i - 1);
}

static uint64_t pixel_index_input(size_t i, size_t j, struct image image) {
	return i * image.width + j;
}

struct image rotate(const struct image image) {
	if (image.pixels == NULL) {
		return (struct image) {.width = image.width, .height = image.height, .pixels = NULL};
	}
	struct pixel* pixels = malloc(sizeof(struct pixel) * image.width * image.height);
	uint64_t input_index;
	uint64_t output_index;
	for (size_t i = 0; i < image.height; i++) {
		for (size_t j = 0; j < image.height; j++) {
			input_index = pixel_index_input(i, j, image);
			output_index = pixel_index_output(i, j, image);
			pixels[output_index] = image.pixels[input_index];
		}
	}
	return (struct image) {.height = image.height, .width = image.width, .pixels = pixels};}

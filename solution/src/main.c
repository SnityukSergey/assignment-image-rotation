#include <stdio.h>
#include "include/bmp.h"
#include "include/image.h"
#include "include/input.h"
#include "include/rotate.h"

#include <stdbool.h>
#include <stdint.h>

static void clean_up(struct image image, FILE* file) {
	image_destroy(&image);
	close_file(file);
}

int main(int argc, char **argv) {
	if (argc != 3) {
		return 1;
	}
	FILE* input = NULL;
	FILE* output = NULL;
	struct image image = {0};
	char* in_name = argv[1];
	char* out_name = argv[2];

	bool read_success = open_file(&input, in_name, "r");
	if (!read_success)
		return 1;
	struct image new_image = rotate(image);
	clean_up(image, input);
	if (new_image.pixels == 0)
		return 1;
	bool open_success = open_file(&output, out_name, "w");
	if (!open_success)
		return 1;
	bool write_success = to_bmp(output, &new_image);
	if (!write_success)
		return 1;
	clean_up(new_image, output);
	return 0;
}
